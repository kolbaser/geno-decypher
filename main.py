import cv2 as cv
import numpy as np
from matplotlib import pyplot as plt
import string
import sys
RS_COEFF = 1

img_rgb = cv.imread('test/'+sys.argv[1])
img_gray = cv.cvtColor(img_rgb, cv.COLOR_BGR2GRAY)
sz = np.shape(img_gray)
img_gray = cv.resize(img_gray, (sz[1] // RS_COEFF, sz[0] // RS_COEFF))

img_gray = cv.blur(img_gray, (4,4)) 

ans=img_rgb.copy();


for letter in string.ascii_lowercase:
#for letter in "azxc":
    template_og = cv.imread('cypher/'+letter+'.png',0)

    for i in range(int(sys.argv[2]) // RS_COEFF, 40 // RS_COEFF):
        template = cv.resize(template_og, (i,i))
        w, h = template.shape[::-1]
        res = cv.matchTemplate(img_gray,template,cv.TM_CCOEFF_NORMED)
        threshold = 0.82
        if letter == 'q': 
            threshold += 0.07
        if letter == 't': 
            threshold -= 0.07
        if letter == 'n': 
            threshold += 0.05
        if letter == 'w': 
            threshold -= 0.05
        if letter == 'q': 
            threshold += 0.05
        if letter == 'b': 
            threshold += 0.04

        loc = np.where( res >= threshold)
        cnt = 0
        for pt in zip(*loc[::-1]):
            cnt += 1
            cv.rectangle(ans, pt, (pt[0] + w + 2, pt[1] + h + 2), (0,0,160), -1)
            cv.putText(ans, letter, (pt[0], pt[1] + h), cv.FONT_HERSHEY_SIMPLEX, 1, (255,255,255), 1, cv.LINE_AA, False)
        if cnt >= 1:
            break
    print(letter + ' ✅')

cv.imwrite('res.png',ans)
