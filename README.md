# Usage
``python3 main.py IMAGE.png SIZE``

The first argument is your source image name, it should be located in the ``/test`` directory.
The second argument is the estimated size of the letter (SIZE x SIZE) in px. 
The resulting edited image (``res.png``) will appear in the same directory as the main.py file.

# Example of usage

``python3 main.py test7.png 22; open res.png``

# Dependencies

``pip3 install matplotlib``
``pip3 install opencv-python``
``pip3 install numpy``
