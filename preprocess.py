import cv2 as cv
import numpy as np
import sys
import string
from matplotlib import pyplot as plt

for i in string.ascii_lowercase+"0123456789-,.?":
    img_letter = cv.imread('./cypher/'+i+'.png')
    img_letter = cv.cvtColor(img_letter, cv.COLOR_BGR2GRAY)
    img_letter = cv.resize(img_letter, (10, 10))
    ret,thresh = cv.threshold(img_letter,100,255,cv.THRESH_BINARY)
    cv.imwrite('./cypher/'+i+'_gray.png',thresh)
exit
